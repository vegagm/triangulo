#!/usr/bin/env pythoh3
# *************************************************************************
#                       IGSAM              InformaticaI
# *************************************************************************
#                          TRIÁNGULO
# *************************************************************************
#                      Vega González Martín
# *************************************************************************

import sys

def line(number: int):
    linea = str(number)*number
    return linea
def triangle(number: int):

    if number >= 9:
        raise ValueError("El número debe ser menor de 9.")
    triangulo = " "

    for i in range(1, number + 1):
            triangulo = str(triangulo) + line(i) + "\n"
    return triangulo

def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()

    

